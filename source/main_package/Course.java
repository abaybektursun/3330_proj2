//This code is very self explanatory
package main_package;

import java.util.ArrayList;

public class Course 
{
	private String code;
	private String name;
	private String prerequisites;
	private String offered_semesters;
	// Courses that have this course as a prerequisite
	public  ArrayList<Course> requisite_to;

	public Course(String code, String name, String prerequisites, String offered_semesters)
	{
		this.code = code;
		this.name = name;
		this.prerequisites     = prerequisites;
		this.offered_semesters = offered_semesters;
		this.requisite_to = new ArrayList<Course>();
	}
	
	public String get_code()
	{
		return this.code;
	}
	public String get_name()
	{
		return this.name;
	}
	public String get_prerequisites()
	{
		return this.prerequisites;
	}
	
	public String get_offered_semesters()
	{
		return this.offered_semesters;
	}
	
	public void set_code(String code)
	{
		this.code = code;
	}
	public void set_name(String name)
	{
		this.name = name;
	}
	public void set_prerequisites(String prerequisites)
	{
		this.prerequisites = prerequisites;
	}
    public void set_offered_semesters(String offered_semesters)
	{
		this.offered_semesters = offered_semesters;
	}
}
