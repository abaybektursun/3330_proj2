package main_package;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Random;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Master {
	public static ArrayList<Course> t_order_stack;

	private static final String All_Courses = null;



	public static void main(String[] args) throws FileNotFoundException {
		// Maybe use args?
		
		ArrayList<Course> courses = new ArrayList<Course>();
		// Instantiate the stack for topologicalo order
		t_order_stack = new ArrayList<Course>();
		
		// Parse the file and populate 'courses' with all the given courses
		courses = courses_parser("../data/courses.txt");
		
		// Populate 'requisite_to' filed for every course
		assign_edges(courses);
		
		// Get the topological sort
		topological_sort(courses);
		
		// Reverse the order of stack
		Collections.reverse(t_order_stack);
		
		// Visualize courses
		sigmajs_export(courses,"../data/template.html");
		
		// Visualize topological order
		sigmajs_export_ordered(t_order_stack, "../data/template.html");
	}
	
	// Pythonian way to System.out.print
	public static void print(String str)
	{
		for(String a_line : str.split("\n"))
		{
			
			System.out.println(a_line);
		}
	}
	
	// Parses text file using regular expressions, then creates list of Courses out of the data in the text
	public static ArrayList<Course> courses_parser(String data_file_name)
	{
		//Find the course codes 
		String pattern = "(MATH|CSCI)( )\\d{4}";
		// Create a Pattern object
	    Pattern r = Pattern.compile(pattern);

        // This will reference one line at a time
        String line = null;
        // An element of this list will represent a course
        ArrayList<String> file_string = new ArrayList<String>();
        
        // Standard file reader-----------------------------------------------
	    try 
	    {
            FileReader fileReader = new FileReader(data_file_name);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) 
            {
            	// prevent cases like CSCI 2320 and MATH 2330, use comma delimiter
            	file_string.add(line.replace(" and ", ","));
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) 
	    {
            print("Failed to Locate: '" + data_file_name + "'");                
        }
        catch(IOException ex) 
	    {
        	ex.printStackTrace();
        }
	    //--------------------------------------------------------------------
	    
	    
	    // Add the delimiters-------------------------------------------------------------------------------------------
	    ArrayList<String> delimeted_file = new ArrayList<String>();
	    for(String a_line : file_string)
	    {
	    	//a_line.replaceFirst("(MATH|CSCI) \\d{4}","|");
	    	Matcher m = r.matcher(a_line);
	    	if (m.find( )) 
	    	{
	    		// We are basically making data delimited here
	    		delimeted_file.add( a_line.replace(m.group(0), m.group(0) + "|").replace("(", "|").replace(")", "|") );
	        } 
	    	else
	        {
	            print("NO MATCH");
	        }
	    }
	    //---------------------------------------------------------------------------------------------------------------
		
	    // Write delimited data into list of courses
	    ArrayList<String> split_line = new ArrayList<String>();
	    ArrayList<Course> Courses    = new ArrayList<Course>();
	    for (String a_line : delimeted_file)
	    {
	    	// Gosh, wasted so much time trying to figure out this
	    	//.split takes regex not just string, so have to use two escape characters
	    	// one for Java another for the regex
	    	for( String data_element : a_line.split("\\|") )
	    	{
	    		//split_line.add(data_element.replace(" ", ""));
	    		split_line.add(data_element);
	    	}

	    	// Create a Course
	    	if (split_line.size() == 4) 
	    	{
	    		Courses.add(new Course( split_line.get(0), split_line.get(1), split_line.get(2), split_line.get(3) ));
	    	}
	    	// Free the list for the next course
	    	split_line.clear();
	    }
	    //-------------------------------------------
	    
		return Courses;
	}

	public static void assign_edges(ArrayList<Course> courses)
	{
		for(Course a_requisite : courses)
		{
			for(Course a_course : courses)
			{
				// See whether 
				if ( a_course.get_prerequisites().contains(a_requisite.get_code()) )
				{
					a_requisite.requisite_to.add(a_course);
				}
			}
		}
	}
	
	public static void topological_sort(ArrayList<Course> courses)
	{
		ArrayList<Course> visited_courses = new ArrayList<Course>();
		
		for(Course a_course : courses)
		{
			// See if the course was visited or not
			if(visited_courses.contains(a_course) == false)
			{
				// Recursively Explore
				explore(visited_courses, a_course);	
			}
		}
	}
	
	public static void explore(ArrayList<Course> visited_courses, Course course)
	{
		// Mark as explored
		visited_courses.add(course);
		//Need to add previsit here
		for(Course a_course : course.requisite_to)
		{
			if(visited_courses.contains(a_course) == false)
			{
				// Recursively Explore
				explore(visited_courses, a_course);	
			}
			// Need to add psot visit here
		}
		t_order_stack.add(course);
	}
		
	public static void sigmajs_export(ArrayList<Course> courses, String orig_fileName)
	{
		// Original HTML file
		ArrayList<String> orig_file_str = new ArrayList<String>();
		
		String line = null;
		// Standard file reader-----------------------------------------------
	    try
	    {
            FileReader fileReader = new FileReader(orig_fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) 
            {
            	orig_file_str.add(line);
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) 
	    {
            print("Failed to Locate: '" + orig_fileName + "'");                
        }
        catch(IOException ex) 
	    {
        	ex.printStackTrace();
        }
	    //--------------------------------------------------------------------
		
		// This is where new file will be stored
		String fileStr = new String();
		// Read the original Text file
		for(String a_line : orig_file_str)
		{
			// When this encountered stard creating nodes
			if (a_line.matches("(// START PUSHING HERE <'UCA36' PAPA JOHN'S 40% DISCOUNT>)+") )
			{
				int i = 0;
				int x_co = 0;
				for(Course a_course : courses)
				{
					fileStr += "\ng.nodes.push({\n";
					fileStr += "	id: '"    + a_course.get_code() + "',\n";
					fileStr += "	label: '" + a_course.get_name() + "',\n";
					fileStr += "	x: Math.random()" + "" + ",\n";
					fileStr += "	y: Math.random()" + " * 1" + ",\n";
					fileStr += "	size:" + a_course.get_code().charAt(6) + "" + ",\n";
					fileStr += "	color: '#666'\n";
					fileStr += "});\n";
					
					i += 1;
					x_co += 7;
				}
			}
			
			// Create Edges
			else if(a_line.matches("(// SERIOUSLY USE PROMO CODE 'UCA36' TO GET 40% OFF ANYTHING IN PAPA JOHN'S, IT WILL ALSO HELP COMPUTER SCIENCE CLUB)+") )
			{
				int id = 0;
				for(Course a_course : courses)
				{
					for(Course a_requisite_to_course : a_course.requisite_to)
					{
						fileStr += "\ng.edges.push({\n";
						fileStr += "	id: " + id + ",\n";
						fileStr += "	source: '" + a_course.get_code() + "',\n";
						fileStr += "	target: '" + a_requisite_to_course.get_code() + "',\n";
						fileStr += "	size: " + "60" + ",\n";
						fileStr += "	color: '#ccc',\n";
						fileStr += "	type: 'arrow'\n";
						fileStr += "});\n";
						// Change ID
						id += 1;
					}
				}
				
			}
			else
			{
				fileStr += a_line + "\n";
			}
		}
		
		// Create HTML SigmaJS Visualization
		try {
			File file = new File("../visualization/index.html");
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(fileStr);
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//----------------------------------------------------------------
	public static void sigmajs_export_ordered(ArrayList<Course> order_stack, String orig_fileName)
	{
		// Original HTML file
		ArrayList<String> orig_file_str = new ArrayList<String>();
		
		String line = null;
		// Standard file reader-----------------------------------------------
	    try
	    {
            FileReader fileReader = new FileReader(orig_fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) 
            {
            	orig_file_str.add(line);
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) 
	    {
            print("Failed to Locate: '" + orig_fileName + "'");                
        }
        catch(IOException ex) 
	    {
        	ex.printStackTrace();
        }
	    //--------------------------------------------------------------------
		
		// This is where new file will be stored
		String fileStr = new String();
		// Read the original Text file
		for(String a_line : orig_file_str)
		{
			// When this encountered stard creating nodes
			if (a_line.matches("(// START PUSHING HERE <'UCA36' PAPA JOHN'S 40% DISCOUNT>)+") )
			{
				int i = 0;
				int x_co = 0;
				for(Course a_course : order_stack)
				{
					fileStr += "\ng.nodes.push({\n";
					fileStr += "	id: '"    + a_course.get_code() + "',\n";
					fileStr += "	label: '" + a_course.get_name() + "',\n";
					fileStr += "	x: " + x_co + ",\n";
					fileStr += "	y: Math.random()" + " * 10 " + ",\n";
					fileStr += "	size:" + a_course.get_code().charAt(6) + "" + ",\n";
					fileStr += "	color: '#666'\n";
					fileStr += "});\n";
					
					i += 1;
					x_co += 10;
				}
			}
			
			// Create Edges
			else if(a_line.matches("(// SERIOUSLY USE PROMO CODE 'UCA36' TO GET 40% OFF ANYTHING IN PAPA JOHN'S, IT WILL ALSO HELP COMPUTER SCIENCE CLUB)+") )
			{
				int id = 0;
				// Need to access using inex then use i + 1
				//for(Course a_course : order_stack)
				for(int i = 0; i < order_stack.size() - 1; i++)	
				{
					fileStr += "\ng.edges.push({\n";
					fileStr += "	id: " + id + ",\n";
					fileStr += "	source: '" + order_stack.get(i).get_code()   + "',\n";
					fileStr += "	target: '" + order_stack.get(i+1).get_code() + "',\n";
					fileStr += "	size: " + "60" + ",\n";
					fileStr += "	color: '#ccc',\n";
					fileStr += "	type: 'arrow'\n";
					fileStr += "});\n";
					// Change ID
					id += 1;
				}
			}
			else
			{
				fileStr += a_line + "\n";
			}
		}
		
		// Create HTML SigmaJS Visualization
		try {
			File file = new File("../visualization/index2.html");
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(fileStr);
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
